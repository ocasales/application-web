package com.application.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.log4j.Log4j2;

@Log4j2
@SpringBootApplication
public class ApplicationWebApplication {
		
	public static void main(String[] args) {
		
		log.info("************************************************");
		SpringApplication.run(ApplicationWebApplication.class, args);
	}

}
